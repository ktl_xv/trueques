const {ethers} = require('@nomiclabs/buidler');

const storeAddress = '0x7c2C195CD6D34B8F845992d380aADB2730bB9C6F';

async function main() {
  // We get the contract to deploy
  const store = await ethers.getContractAt('Store', storeAddress);
  const bordando = await store.newStore(
    'ipfs://QmNgnY2TJ3hHpVpbUYyXdJMGy4QDmUY9ZrBDLBSd2szGrT'
  );
  console.log(bordando);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
