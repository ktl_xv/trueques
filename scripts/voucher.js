const {ethers} = require('@nomiclabs/buidler');

const storeId = 1;
const voucherAddress = '0x8858eeB3DfffA017D4BCE9801D340D36Cf895CCf';

async function main() {
  // We get the contract to deploy
  const voucher = await ethers.getContractAt('Voucher', voucherAddress);
  const bordado = await voucher.construct(
    storeId,
    'ipfs://QmXefoZW54QSF3MoPvHMTWdxRdymjYBvbXcULarwd1c9uM'
  );
  console.log(bordado);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
