const {ethers} = require('@nomiclabs/buidler');

const storeAddress = '0x7c2C195CD6D34B8F845992d380aADB2730bB9C6F';
const storeId = 1;

async function main() {
  // We get the contract to deploy
  const store = await ethers.getContractAt('Store', storeAddress);
  const stateBefore = await store.validated(storeId);
  console.log(`The state of Store ${storeId} is ${stateBefore}`);
  await store.validate(1);
  const stateAfter = await store.validated(storeId);
  console.log(`The state of Store ${storeId} is ${stateAfter}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
