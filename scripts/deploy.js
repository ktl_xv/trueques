const {ethers} = require('@nomiclabs/buidler');

async function main() {
  // We get the contract to deploy
  const Store = await ethers.getContractFactory('Store');
  const store = await Store.deploy();
  await store.deployed();

  console.log(`Store deployed to: ${store.address}`);

  const Voucher = await ethers.getContractFactory('Voucher');
  const voucher = await Voucher.deploy(store.address);
  await voucher.deployed();

  console.log(`Voucher deployed to: ${voucher.address}`);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
