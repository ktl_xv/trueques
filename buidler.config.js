/* global usePlugin */

usePlugin('@nomiclabs/buidler-waffle');
usePlugin('solidity-coverage');
usePlugin('@nomiclabs/buidler-solhint');

module.exports = {
  solc: {
    version: '0.6.12',
  },
  paths: {
    sources: './src/contracts',
    tests: './test/contracts',
    artifacts: './src/artifacts',
  },
};
