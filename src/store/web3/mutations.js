// eslint-disable no-param-reassign
export function SET_PROVIDER(state, provider) {
  state.provider = provider;
}
export function SET_COINBASE(state, coinbase) {
  state.coinbase = coinbase;
}
export function SET_CHAIN_ID(state, chainId) {
  state.chainId = chainId;
}
export function SET_CONNECTED(state, connected) {
  state.connected = connected;
}
export function SET_BALANCE(state, balance) {
  state.balance = balance;
}
