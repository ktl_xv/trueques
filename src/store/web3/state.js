export default function state() {
  return {
    isInjected: false,
    provider: null,
    chainId: null,
    coinbase: '',
    balance: null,
    connected: false,
    error: null,
    storeAddress: '0x7c2C195CD6D34B8F845992d380aADB2730bB9C6F',
    voucherAddress: '0x8858eeB3DfffA017D4BCE9801D340D36Cf895CCf',
  };
}
