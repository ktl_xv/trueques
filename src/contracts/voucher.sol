// SPDX-License-Identifier: MIT

pragma solidity ^0.6.0;

import '@openzeppelin/contracts/utils/Counters.sol';

import './TruequesLib.sol';
import './ERC1155.sol';
import './store.sol';

import '@nomiclabs/buidler/console.sol';

contract Voucher is ERC1155 {
  using Counters for Counters.Counter;
  Counters.Counter private _voucherIds;

  Store public store;

  mapping(uint256 => string) private _uris;
  mapping(uint256 => uint256) public voucherStore;
  mapping(uint256 => uint256[]) public storeVouchers;
  mapping(uint256 => uint256) public minted;
  mapping(uint256 => uint256) public burned;

  constructor(address _storeContract) public ERC1155('') {
    store = Store(_storeContract);
  }

  modifier voucherExists(uint256 voucherId) {
    uint256 maxVoucherId = _voucherIds.current();
    require(voucherId <= maxVoucherId, 'Voucher does not exist');
    _;
  }

  modifier senderIsStoreOwner(uint256 storeId) {
    address storeOwner = store.ownerOf(storeId);
    require(_msgSender() == storeOwner, 'Sender is not owner of Store');
    _;
  }

  modifier minterIsStoreOwner(uint256 voucherId) {
    uint256 storeId = voucherStore[voucherId];
    address storeOwner = store.ownerOf(storeId);
    require(_msgSender() == storeOwner, 'Minter is not owner of Store');
    _;
  }

  function construct(uint256 storeId, string memory jsonURI)
    external
    senderIsStoreOwner(storeId)
    returns (uint256)
  {
    require(TruequesLib.isIPFSAddr(jsonURI), 'Invalid IPFS URI');
    require(store.validated(storeId), 'Store has not been validated');

    _voucherIds.increment();

    uint256 newVoucherId = _voucherIds.current();

    _uris[newVoucherId] = jsonURI;
    voucherStore[newVoucherId] = storeId;
    storeVouchers[storeId].push(newVoucherId);
    emit URI(jsonURI, newVoucherId);

    return newVoucherId;
  }

  function uri(uint256 voucherId)
    external
    override
    view
    voucherExists(voucherId)
    returns (string memory)
  {
    return _uris[voucherId];
  }

  function mint(
    address to,
    uint256 id,
    uint256 amount
  ) public voucherExists(id) minterIsStoreOwner(id) {
    require(amount > 0, 'Ammount of minted vouchers should be greater than 0');
    _mint(to, id, amount, '');
    minted[id] = minted[id] + amount;
  }

  function storeVoucherCount(uint256 storeId) public view returns (uint256) {
    return storeVouchers[storeId].length;
  }
}
