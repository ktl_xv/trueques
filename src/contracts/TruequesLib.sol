// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

library TruequesLib {
  function isIPFSAddr(string memory str) internal pure returns (bool) {
    bytes memory bStr = bytes(str);
    return (bStr.length == 53 &&
      bStr[0] == 'i' &&
      bStr[1] == 'p' &&
      bStr[2] == 'f' &&
      bStr[3] == 's' &&
      bStr[4] == ':' &&
      bStr[5] == '/' &&
      bStr[6] == '/' &&
      bStr[7] == 'Q' &&
      bStr[8] == 'm');
  }
}
