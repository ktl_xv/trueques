// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

import '@openzeppelin/contracts/token/ERC721/ERC721.sol';
import '@openzeppelin/contracts/utils/Counters.sol';
import '@openzeppelin/contracts/access/AccessControl.sol';

import './TruequesLib.sol';

import '@nomiclabs/buidler/console.sol';

contract Store is ERC721, AccessControl {
  using Counters for Counters.Counter;
  Counters.Counter public _tokenIds;

  mapping(uint256 => bool) public validated;
  mapping(uint256 => bool) public hidden;
  mapping(uint256 => uint256[]) public vouchers;

  bytes32 public constant MINTER_ROLE = keccak256('MINTER_ROLE');
  bytes32 public constant VALIDATOR_ROLE = keccak256('VALIDATOR_ROLE');

  /**
   * @dev See {_setURI}.
   */
  constructor() public ERC721('TruequesStore', 'TKST') {
    _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
    _setupRole(MINTER_ROLE, _msgSender());
    _setupRole(VALIDATOR_ROLE, _msgSender());
  }

  function newStore(string memory jsonURI) public returns (uint256) {
    require(TruequesLib.isIPFSAddr(jsonURI), 'Invalid IPFS URI');
    _tokenIds.increment();

    uint256 newStoreId = _tokenIds.current();

    _safeMint(_msgSender(), newStoreId);
    _setTokenURI(newStoreId, jsonURI);

    return newStoreId;
  }

  function validate(uint256 storeId) public {
    require(hasRole(VALIDATOR_ROLE, _msgSender()), 'Caller is not a validator');
    require(storeId <= _tokenIds.current(), 'Store does not exist');
    validated[storeId] = true;
  }

  function hide(uint256 storeId) public {
    require(ownerOf(storeId) == _msgSender(), 'Caller is not the owner');
    hidden[storeId] = true;
  }

  function unhide(uint256 storeId) public {
    require(ownerOf(storeId) == _msgSender(), 'Caller is not the owner');
    hidden[storeId] = false;
  }
}
