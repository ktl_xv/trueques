const routes = [
  {
    name: 'home',
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {name: 'home', path: '', component: () => import('pages/Index.vue')},
      {name: 'list', path: 'list', component: () => import('pages/List.vue')},
      {path: 'user', component: () => import('pages/MyVouchers.vue')},
      {path: 'new', component: () => import('pages/New.vue')},
      {
        path: '/profile/:storeId',
        component: () => import('pages/Profile.vue'),
        children: [
          {
            name: 'profile',
            path: '/',
            component: () => import('components/ProfileVoucherList.vue'),
          },
          {
            name: 'define',
            path: 'define',
            component: () => import('components/Define.vue'),
          },
          {
            name: 'voucher',
            path: 'voucher/:voucherId',
            component: () => import('components/VoucherDetail.vue'),
          },
        ],
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
