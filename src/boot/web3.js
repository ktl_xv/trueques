import Vue from 'vue';
import Web3 from 'web3';

const web3 = new Web3(window.ethereum || 'https://testnet.19930528.xyz');

Vue.prototype.$web3 = {instance: web3};
