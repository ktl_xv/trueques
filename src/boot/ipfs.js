import Vue from 'vue';
import IPFS from 'ipfs';
import FileType from 'file-type';
import toBuffer from 'it-to-buffer';
import OrbitDB from 'orbit-db';

export default async () => {
  const ipfs = await IPFS.create({
    EXPERIMENTAL: {pubsub: true},
  });
  ipfs.swarm.connect(
    '/dns4/ws.ipfs.ktlxv.com/tcp/443/wss/p2p/QmexZyYyzVFpZikAabvJPrsC6AMCNGnMPmK3y6EfzJf85c',
    '/ip4/192.168.1.50/tcp/38427/ipfs/QmekmeMuVVtgF2kuQ9uAmDndZMcvn7dKFceq1TrcYvoGMn'
  );
  ipfs.getJson = async function json(hash) {
    const stream = this.cat(hash, {timeout: 10 * 1000});
    const buffer = await toBuffer(stream);
    return JSON.parse(buffer.toString());
  };

  ipfs.getImage = async function json(hash) {
    const stream = this.cat(hash);
    const buffer = await toBuffer(stream);
    const fileType = await FileType.fromBuffer(buffer);
    // TODO: Check if it is really an image
    return `data:${fileType.mime};base64,${buffer.toString('base64')}`;
  };

  const receiveMsg = (msg) => console.log(msg.data.toString());

  await ipfs.pubsub.subscribe((await ipfs.id()).id, receiveMsg);

  const orbitdb = await OrbitDB.createInstance(ipfs);

  Vue.prototype.$ipfs = ipfs;
  Vue.prototype.$orbitdb = orbitdb;
};
