module.exports = {
  presets: ['@quasar/babel-preset-app'],
  plugins: ['@babel/plugin-proposal-async-generator-functions']
};
