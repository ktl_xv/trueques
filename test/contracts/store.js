const {expect, use} = require('chai');
const {ethers} = require('@nomiclabs/buidler');
const keccak256 = require('keccak256');

const {solidity} = require('ethereum-waffle');

use(solidity);

describe('Store', function () {
  let Store;
  let store;
  const storeId = 1;
  let owner;
  let addr1;
  const exampleIPFSAddr =
    'ipfs://QmV8cfu6n4NT5xRr2AHdKxFMTZEJrA44qgrBCr739BN9Wb';

  before(async function deploy() {
    Store = await ethers.getContractFactory('Store');
    store = await Store.deploy();

    [owner, addr1] = await ethers.getSigners();

    await store.deployed();
  });

  it('Should set MINTER_ROLE to deployer account', async function () {
    const minterRoleHash = keccak256('MINTER_ROLE');
    expect(await store.hasRole(minterRoleHash, await owner.getAddress())).to.be
      .true;
  });

  it('Should set VALIDATOR_ROLE to deployer account', async function () {
    const minterRoleHash = keccak256('VALIDATOR_ROLE');
    expect(await store.hasRole(minterRoleHash, await owner.getAddress())).to.be
      .true;
  });

  it('Should allow anyone to mint new stores', async function () {
    await expect(store.newStore(exampleIPFSAddr)).to.be.ok;
    await expect(store.connect(addr1).newStore(exampleIPFSAddr)).to.be.ok;
  });

  it('Should emmit a transfer when minting new stores', async function () {
    await expect(store.newStore(exampleIPFSAddr))
      .to.emit(store, 'Transfer')
      .withArgs(
        '0x0000000000000000000000000000000000000000',
        await owner.getAddress(),
        2
      );
  });

  it('Should not allow to mint a new store with an invalid IPFS URI', async function () {
    await expect(store.newStore('')).to.be.revertedWith('Invalid IPFS URI');
  });

  it('Should be invalid on creation', async function () {
    expect(await store.validated(storeId)).to.be.false;
  });

  it('Should allow only VALIDATORs to validate a store', async function () {
    await expect(store.connect(addr1).validate(storeId)).to.be.revertedWith(
      'Caller is not a validator'
    );

    await expect(store.validate(storeId)).to.be.ok;
  });

  it('Should be valid after validation', async function () {
    expect(await store.validated(storeId)).to.be.true;
  });

  it('Should only allow existing stores to be validated', async function () {
    await expect(store.validate(100)).to.be.revertedWith(
      'Store does not exist'
    );
  });

  it('Should not be hidden on creation', async function () {
    expect(await store.hidden(storeId)).to.be.false;
  });

  it('Should allow only the owner to hide a store', async function () {
    await expect(store.connect(addr1).hide(storeId)).to.be.revertedWith(
      'Caller is not the owner'
    );

    await expect(store.hide(storeId)).to.be.ok;
  });

  it('Should be hidden after hiding it', async function () {
    expect(await store.hidden(storeId)).to.be.true;
  });

  it('Should allow only the owner to unhide a store', async function () {
    await expect(store.connect(addr1).unhide(storeId)).to.be.revertedWith(
      'Caller is not the owner'
    );

    await expect(store.unhide(storeId)).to.be.ok;
  });

  it('Should not be hidden after unhidding it', async function () {
    expect(await store.hidden(storeId)).to.be.false;
  });
});
