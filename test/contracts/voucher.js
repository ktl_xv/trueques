const {expect} = require('chai');
const {ethers} = require('@nomiclabs/buidler');
const {smockit} = require('@eth-optimism/smock');

describe('Voucher', function () {
  let Voucher;
  let voucher;
  let mockStore;
  const storeId = 1;
  let storesContractAddress;
  let owner;
  let addr1;
  let voucherCounter = 0;
  const exampleIPFSAddr =
    'ipfs://QmV8cfu6n4NT5xRr2AHdKxFMTZEJrA44qgrBCr739BN9Wb';
  before(async function deploy() {
    [owner, addr1] = await ethers.getSigners();

    const Store = await ethers.getContractFactory('Store');
    mockStore = smockit(Store, owner);
    storesContractAddress = mockStore.address;

    mockStore.smocked.ownerOf.will.return.with(await owner.getAddress());
    mockStore.smocked.validated.will.return.with((_storeId) => {
      return _storeId.eq(storeId);
    });

    Voucher = await ethers.getContractFactory('Voucher');
    voucher = await Voucher.deploy(storesContractAddress);
  });

  it('Should store the Stores contract address', async function () {
    expect(await voucher.store()).to.equal(storesContractAddress);
  });

  it('Should only allow owners of Store to construct a token in their store', async function () {
    await expect(
      voucher.connect(addr1).construct(storeId, exampleIPFSAddr)
    ).to.be.revertedWith('Sender is not owner of Store');
    expect(await voucher.construct(storeId, exampleIPFSAddr)).to.be.ok;
    voucherCounter += 1;
  });

  it('Should store the Store a token belongs to', async function () {
    expect(await voucher.voucherStore(voucherCounter)).to.equal(storeId);
  });

  it('Should not allow to construct a new voucher with an invalid IPFS URI', async function () {
    await expect(voucher.construct(storeId, '')).to.be.revertedWith(
      'Invalid IPFS URI'
    );
  });

  it('Should only allow validated Stores to construct vouchers', async function () {
    await expect(
      voucher.construct(storeId + 1, exampleIPFSAddr)
    ).to.be.revertedWith('Store has not been validated');
  });

  it('Should emit a URI event when constructing', async function () {
    expect(await voucher.construct(storeId, exampleIPFSAddr));
    voucherCounter += 1;
  });

  it('Should return the correct URI for a token', async function () {
    expect(await voucher.uri(voucherCounter)).to.equal(exampleIPFSAddr);
  });

  it('Should revert when asking the URI for a nonexisting voucher', async function () {
    await expect(voucher.uri(voucherCounter + 1)).to.be.revertedWith(
      'Voucher does not exist'
    );
  });

  it('Should allow only the owner of a store to mint its vouchers', async function () {
    const mintedVouchers = 1;
    await expect(
      voucher
        .connect(addr1)
        .mint(await addr1.getAddress(), voucherCounter, mintedVouchers)
    ).to.revertedWith('Minter is not owner of Store');
    await expect(
      voucher.mint(await addr1.getAddress(), voucherCounter, mintedVouchers)
    ).to.be.ok;

    expect(
      await voucher.balanceOf(await addr1.getAddress(), voucherCounter)
    ).to.equal(mintedVouchers);
  });

  it('Should only allow minting of constructed vouchers', async function () {
    await expect(
      voucher.mint(await addr1.getAddress(), voucherCounter + 1, 1)
    ).to.be.revertedWith('Voucher does not exist');
  });

  it('Should only allow to mint more than 0 vouchers', async function () {
    await expect(
      voucher.mint(await addr1.getAddress(), voucherCounter, 0)
    ).to.be.revertedWith('Ammount of minted vouchers should be greater than 0');
  });
});
